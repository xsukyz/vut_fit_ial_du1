# VUT_FIT_IAL_du1
* ***c202/c202.c*** - stack
* ***c204/c204.c*** - infix to postfix
* ***c206/c206.c*** - doubly linked list

### Results
 - 9/10
 #### Errors
 My advanced test output with errors:

```
[TEST39]
We add a new element at the begin by DLPreInsert.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-----------------
 	23	 <= activity 
 	20
-----------------
Return value of function DLCopyFirst is 23.
Return value of function DLCopy is 23.
Return value of function DLActive is TRUE.

[TEST40]
Function DLPostDelete at the end of the list should do nothing.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-----------------
 	23
 	20	 <= activity 
-----------------
-----------------
 	23
 	20	 <= activity 
-----------------
Return value of function DLActive is TRUE.

[TEST41]
We add a new element at the end by DLPostInsert.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-----------------
 	23
 	20	 <= activity 
 	0
-----------------
Return value of function DLCopyLast is 20.
Return value of function DLCopy is 20.
Return value of function DLActive is TRUE.
```
Correct output:
```
[TEST39]
We add a new element at the begin by DLPreInsert.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-----------------
 	26
 	23	 <= activity 
 	20
-----------------
Return value of function DLCopyFirst is 26.
Return value of function DLCopy is 23.
Return value of function DLActive is TRUE.

[TEST40]
Function DLPostDelete at the end of the list should do nothing.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-----------------
 	26
 	23
 	20	 <= activity 
-----------------
-----------------
 	26
 	23
 	20	 <= activity 
-----------------
Return value of function DLActive is TRUE.

[TEST41]
We add a new element at the end by DLPostInsert.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-----------------
 	26
 	23
 	20	 <= activity 
 	27
-----------------
Return value of function DLCopyLast is 27.
Return value of function DLCopy is 20.
Return value of function DLActive is TRUE.
```


